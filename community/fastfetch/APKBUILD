# Contributor: Carter Li <zhangsongcui@live.cn>
# Maintainer: fossdd <fossdd@pwned.life>
pkgname=fastfetch
pkgver=2.34.0
pkgrel=0
pkgdesc="Maintained, feature-rich and performance oriented, neofetch-like system information tool"
url="https://github.com/fastfetch-cli/fastfetch"
arch="all"
license="MIT"
depends="
	hwdata-pci
	"
makedepends="
	cmake samurai
	yyjson-dev
	yyjson-static
	vulkan-loader-dev
	libxcb-dev
	wayland-dev
	libdrm-dev
	dconf-dev
	imagemagick-dev
	chafa-dev
	zlib-dev
	dbus-dev
	mesa-dev
	opencl-dev
	xfconf-dev
	sqlite-dev
	pulseaudio-dev
	ddcutil-dev
	elfutils-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/fastfetch-cli/fastfetch/archive/refs/tags/$pkgver.tar.gz"

prepare() {
	default_prepare

	rm -rf src/3rdparty/yyjson
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		local crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=Release \
		-DBUILD_FLASHFETCH=OFF \
		-DBUILD_TESTS=ON \
		-DENABLE_SYSTEM_YYJSON=ON \
		-DENABLE_DIRECTX_HEADERS=OFF \
		-DPACKAGES_DISABLE_DPKG=ON \
		-DPACKAGES_DISABLE_EMERGE=ON \
		-DPACKAGES_DISABLE_EOPKG=ON \
		-DPACKAGES_DISABLE_GUIX=ON \
		-DPACKAGES_DISABLE_LINGLONG=ON \
		-DPACKAGES_DISABLE_LPKG=ON \
		-DPACKAGES_DISABLE_LPKGBUILD=ON \
		-DPACKAGES_DISABLE_NIX=ON \
		-DPACKAGES_DISABLE_OPKG=ON \
		-DPACKAGES_DISABLE_PACMAN=ON \
		-DPACKAGES_DISABLE_PACSTALL=ON \
		-DPACKAGES_DISABLE_PALUDIS=ON \
		-DPACKAGES_DISABLE_PKG=ON \
		-DPACKAGES_DISABLE_PKGTOOL=ON \
		-DPACKAGES_DISABLE_RPM=ON \
		-DPACKAGES_DISABLE_SORCERY=ON \
		-DPACKAGES_DISABLE_XBPS=ON \
		$crossopts
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8a2bbcd1f612e1fd4649cd64b0a1b52fe52012adb89cf42f56d16d677ff407a88c1e3992c58fdbbccacb668456fa1288e1667eb631791b37ba2267b2d3021cc5  fastfetch-2.34.0.tar.gz
"
